var express             = require('express');
var router              = express.Router();
var mongoose            = require('mongoose')
var authenticate        = require('../../middleware/authenticate');
var Term_Condiction            = require('../models/terms_condiction');

var ERROR = {
    BAD_REQ : {
        success : false,
        resCode : 400,
        resMessage : "Bad request"
    },
    UNAUTH : {
        success : false,
        resCode : 401,
        resMessage : "User Unauthorized"
    }
}

router.post('/add',authenticate, function(req, res, next) {
  console.log("lang",req.body.lang);
  console.log("Title",req.body.title);
    if(req.body.lang && req.body.title) {

      var terms_condiction_details=new Term_Condiction({
                        lang                  : req.body.lang,
                        title                 : req.body.title,
                        description           : req.body.description,
                        long_description      : req.body.long_description,
                        flag                  : req.body.flag,
                      });
        terms_condiction_details.save(function(err, tc) {
            if (err) throw err;
            if(tc) {
                var response = {
                    success : true,
                    resCode : 200,
                    resMessage : "Terms and condiction added successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});

router.put('/edit/:id',authenticate, function(req, res, next) {
      if(req.body.lang && req.body.title) {
        var details={
                lang                  : req.body.lang,
                title                 : req.body.title,
                description           : req.body.description,
                long_description      : req.body.long_description,
                flag                  : req.body.flag,
        };
        Term_Condiction.update({_id:req.params.id},{$set:details}, function(err, language) {
            if (err) throw err;
            if(language) {
                var response = {
                    success : true,
                    resCode : 200,
                    resMessage : "Terms and condiction edited successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});

router.post('/publish',authenticate, function(req, res, next) {
    if(req.body.flag && req.body.id) {
        var details={
            flag      : req.body.flag
        };
        Term_Condiction.update({_id:req.body.id},{$set:details}, function(err, language) {
            if (err) throw err;
            if(language) {
                var response = {
                    success : true,
                    text    : req.body.flag,
                    resCode : 200,
                    resMessage : "Edited successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});

router.get('/delete/:id',authenticate, function(req, res, next) {
    if(req.params.id) {
        Term_Condiction.remove({_id:req.params.id},function(err, item) {
            if (err) throw err;
            if(item) {
                var response = {
                    success : true,
                    resCode : 200,
                    resMessage : "Deleted successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});


module.exports = router;
