var definedapikey              = "HTBcrQU49Qf7syks5EUuRCFGv3q2Xkfa9";
var ERROR = {
    UNAUTH : {
        success : false,
        resCode : 401,
        resMessage : "Unauthorized Access"
    }
};
function isValidateapi(req, res, next) {
    // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
    // you can do this however you want with whatever variables you set up
    var apikey = req.headers['apikey'];
    if (apikey==definedapikey) {
      next();
    }else{
      res.status(401).json(ERROR.UNAUTH);
    }
}
module.exports=isValidateapi;
