var express             = require('express');
var router              = express.Router();
var jwt                 = require('jsonwebtoken');
var app                 = express();
var crypto              = require('crypto');
var secret              = "HTBcrQU49Qf7syks5EUuRCFGv3q2Xkfa9!nm3q7$$1g%si#!g7#=)mh-v^rt%hf!e#$b&n!5=3c*@37gzy";
var connection          = require('../../config/database');
var mail                = require('../../Lib/sendmail');
var validateapi         = require('../../middleware/validateapi');
/* admin user registration */
router.post('/registerUser', function(req, res) {
    var name    = req.body.name;
    var email   = req.body.email;
    var password = req.body.password;
    
    var hash = crypto.createHash('md5').update(password).digest('hex');
    //console.log(hash);
    var date = new Date(),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    var curentDate = [date.getFullYear(), mnth, day].join("-");

    var response = {
            name    : name,
            email   : email,
            password: hash,
			created: curentDate,
            modified: curentDate
        }
        /********************Using validation Start*****************************/
        

    var validation = [];
    if (name == null || name == '') {
        //validation.push('Please input a name');
        validation[0] = {
            "responseCode": "203",
            "responseMessage": "Please insert a name!"
        };
    }
    if (email == null || email == '') {
        //validation.push('Please input a email');
        validation[1] = {
            "responseCode": "203",
            "responseMessage": "Please insert a email!"
        };
    }
    if (password == null || password == '') {
        //validation.push('Please input a p');
        validation[2] = {
            "responseCode": "203",
            "responseMessage": "Please insert a password!"
        };
    }

    if (validation.length == 0) {
        var qry = "SELECT * from admin where email LIKE '%" + email + "%'";
        var query = connection.query(qry, function(err, rows, fields) {
            if (rows.length == 0) {
                var query = connection.query('INSERT INTO admin SET ?', response, function(err, result) {
                    if (!err) {
                        res.json({
                            responseCode: '200',
                            responseMessage: 'Admin user added successfully'
                        });
                    } else {
                        res.json({
                            responseCode: '201',
                            responseMessage: 'Admin user added not successfully!'
                        });
                    }
                });
            } else {
                res.json({
                    responseCode: '202',
                    responseMessage: 'The user already exit!'
                });
            }
        });
    } else {
        //res.json({ errors: errors });
        res.json({
            responseCode: '203',
            responseMessage: validation
        });
    }
    /******************** Using validation End *****************************/
});

/* admin user login */
router.post('/loginAdmin', function(req, res) {
    var email       = req.body.email;
    var password    = req.body.password;
    var hash = crypto.createHash('md5').update(password).digest('hex');
    console.log(email);
    /********************Using validation Start*****************************/
    var validation = [];
    if (email == null || email == '') {
        validation[1] = {
            "responseCode": "203",
            "responseMessage": "Please insert a email!"
        };
    }
    if (password == null || password == '') {
        validation[2] = {
            "responseCode": "203",
            "responseMessage": "Please insert a password!"
        };
    }

    if (validation.length == 0) {
        var result = [];
        var qry = "SELECT * from admin where email LIKE '%" + email + "%' AND password LIKE '%" + hash + "%'";
        var query = connection.query(qry, function(err, response, fields) {
            result = response;
            if (result.length > 0) {
                res.status(200).json({
                    userParam: result[0],
                    responseCode: "200"
                });
            } else {
                res.json({
                    responseCode: '201',
                    responseMessage: 'Invalid email or password.!'
                });
            }
        });
    } else {
        res.json({
            responseCode: '203',
            responseMessage: validation
        });
    }
    /******************** Using validation End *****************************/
});

/* update admin user */
router.post('/updateAdmin', function(req, res) {
    var id          = req.body.userId;
    var name        = req.body.name;
    var email       = req.body.email;
    var password    = req.body.password;


    var hashPass = '';
    //console.log(hash);
    /********************Using validation Start*****************************/
    var validation = [];
    if (name == null || name == '') {
        //validation.push('Please input a name');
        validation[0] = {
            "responseCode": "203",
            "responseMessage": "Please insert a name!"
        };
    }
    if (email == null || email == '') {
        validation[1] = {
            "responseCode": "203",
            "responseMessage": "Please insert a email!"
        };
    }
    
    if (validation.length == 0) {
        var result = [];
        var qry = "SELECT * from admin where id =" + id;
        var query = connection.query(qry, function(err, response, fields) {
            result = response;
            if (result.length > 0) {
                if (password == '' || password == null) {
                    hashPass = result[0].password;
                } else {
                    hashPass = crypto.createHash('md5').update(password).digest('hex');
                }
                var date = new Date(),
                    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                    day = ("0" + date.getDate()).slice(-2);
                var curentDate = [date.getFullYear(), mnth, day].join("-");

                var userObj = {
                    name        : name,
                    email       : email,
                    password    : hashPass,
                    modified    : curentDate
                };
                // update query
                connection.query('UPDATE admin SET ? WHERE ?', [userObj, {
                        id: id
                    }])
                res.json({
                    responseCode: '200',
                    responseMessage: 'Admin user edited successfully'
                });
            } else {
                res.json({
                    responseCode: '201',
                    responseMessage: 'Some error occure. Please try again!'
                });
            }
        });
    } else {
        res.json({
            responseCode: '203',
            responseMessage: validation
        });
    }
    /******************** Using validation End *****************************/
});
module.exports = router;
