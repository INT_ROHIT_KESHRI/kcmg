var express             = require('express');
var router              = express.Router();
var jwt                 = require('jsonwebtoken');
var app                 = express();
var crypto              = require('crypto');
var secret              = "HTBcrQU49Qf7syks5EUuRCFGv3q2Xkfa9!nm3q7$$1g%si#!g7#=)mh-v^rt%hf!e#$b&n!5=3c*@37gzy";
var connection          = require('../../config/database');
var mail                = require('../../Lib/sendmail');
var validateapi         = require('../../middleware/validateapi');
var ERROR = {
    BAD_REQ : {
        success : false,
        resCode : 400,
        resMessage : "Bad request"
    },
    UNAUTH : {
        success : false,
        resCode : 401,
        resMessage : "User Unauthorized"
    },
    INVALID : {
        success : false,
        resCode : 402,
        resMessage : "Invalid Email Address"
    },
    INVALIDCREDENTIAL : {
        success : false,
        resCode : 403,
        resMessage : "Invalid Credentials"
    }
}
router.post('/emailVarify',validateapi,function(req,res,next){
  var email = req.body.email;
  if(!email){
    res.status(400).json(ERROR.BAD_REQ);
  }else{
    var qry = "SELECT * from users where email ='"+email+"'";
    var query = connection.query(qry, function(err, rows, fields) {
      if(rows.length>0){
        var generatedotp=Math.floor((Math.random() * 10000000) + 1);
        var response={
            success     : true,
            resCode     : 200,
            resMessage  : generatedotp
        };
        mail(email,generatedotp);
        var userObj = {
            otp: generatedotp
        };
        connection.query('UPDATE users SET ? WHERE ?', [userObj, {
                id: rows[0]['id']
            }])
        res.status(200).send(response)
      }else{
        res.status(200).send(ERROR.INVALID);
      }
    });
  }
});
router.post('/resendOtp',validateapi,function(req,res,next){
  var email = req.body.email;
  if(!email){
    res.status(400).json(ERROR.BAD_REQ);
  }else{
    var qry = "SELECT * from users where email ='"+email+"' And otp !=0";
    var query = connection.query(qry, function(err, rows, fields) {
      if(rows.length>0){
        var response={
            success     : true,
            resCode     : 200,
            resMessage  : rows[0]['otp']
        };
        mail(email,rows['otp']);
        res.status(200).send(response)
      }else{
        res.status(403).send(ERROR.INVALIDCREDENTIAL);
      }
    });
  }
});
router.post('/firstlogin',validateapi,function(req,res,next){
  var email     = req.body.email;
  var otp       = req.body.otp;
  var token       = req.body.token;
  var password  = crypto.createHash('md5').update(req.body.pin).digest("hex");
  if(!email || !otp || !password || !token){
    res.status(400).json(ERROR.BAD_REQ);
  }else{
    var qry = "SELECT * from users where email ='"+email+"' And otp ='"+otp+"'";
    var query = connection.query(qry, function(err, rows, fields) {
      if(rows.length>0){
        var userObj = {
            password: password,
            notification_token: token,
            otp     :0,
            status  :'1'
        };
        connection.query('UPDATE users SET ? WHERE ?', [userObj, {
                id: rows[0]['id']
        }]);
         /* Create Accesstoken  remove older one and save in database */
        var token = jwt.sign(rows[0], secret);
        var tokendata = {
                user_id: rows[0]['id'],
                token: token,
                created: new Date(),
                modified: new Date()
        }
        connection.query("DELETE from access_token where user_id= '"+rows[0]['id']+"'", function(err, result) {
          connection.query('INSERT INTO access_token SET ?', tokendata, function(err, result) {
              connection.query('SELECT * FROM master_sector', function(err, response, fields) {
                  if (err) {
                      res.send('Error');
                  } else {
                      var response={
                          success     : true,
                          resCode     : 200,
                          path        : 'http://150.129.179.174:4000/public/sectoricon',
                          accesstoken : token,
                          id          : rows[0]['id'],
                          name        : rows[0]['name'],
                          email       : rows[0]['email'],
                          phone       : rows[0]['phone'],
                          department  :rows[0]['department'],
                          sectorName      :   response
                      };
                      res.status(200).send(response);
                  }
              });
          });
        });
        /* End */
      }else{
        res.status(403).send(ERROR.INVALIDCREDENTIAL);
      }
    });
  }
});
router.post('/login',validateapi,function(req,res,next){
  var email     = req.body.email;
  var token       = req.body.token;
  var password  = crypto.createHash('md5').update(req.body.pin).digest("hex");
  if(!email || !req.body.pin || !token){
    res.status(200).json(ERROR.BAD_REQ);
  }else{
    console.log(password);
    var qry = "SELECT * from users where email ='"+email+"' And password ='"+password+"' And status='1'";
    console.log(qry);
    var query = connection.query(qry, function(err, rows, fields) {
      if(rows.length>0){
         /* Create Accesstoken  remove older one and save in database */
        var token = jwt.sign(rows[0], secret);
        var tokendata = {
                user_id: rows[0]['id'],
                token: token,
                created: new Date(),
                modified: new Date()
        }
        connection.query("DELETE from access_token where user_id= '"+rows[0]['id']+"'", function(err, result) {
          connection.query('INSERT INTO access_token SET ?', tokendata, function(err, result) {
            var userObj = {
              notification_token: token
            };
            connection.query('UPDATE users SET ? WHERE ?', [userObj, {
                    id: rows[0]['id']
            }]);
            connection.query('SELECT * FROM master_sector', function(err, response, fields) {
                if (err) {
                    res.send('Error');
                } else {
                    var response={
                        success     : true,
                        resCode     : 200,
                        accesstoken : token,
                        id          : rows[0]['id'],
                        name        : rows[0]['name'],
                        email       : rows[0]['email'],
                        phone       : rows[0]['phone'],
                        department  :rows[0]['department'],
                        sectorName      :   response,
                        path        : 'http://150.129.179.174:4000/public/sectoricon',
                    };
                    res.status(200).send(response);
                }
            });
          });
        });
        /* End */


      }else{
        res.status(400).send(ERROR.INVALIDCREDENTIAL);
      }
    });
  }
});

module.exports = router;
