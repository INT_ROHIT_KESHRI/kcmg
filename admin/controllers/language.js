var express             = require('express');
var router              = express.Router();
var mongoose            = require('mongoose')
var authenticate        = require('../../middleware/authenticate');
var Language            = require('../models/language');

var ERROR = {
    BAD_REQ : {
        success : false,
        resCode : 400,
        resMessage : "Bad request"
    },
    UNAUTH : {
        success : false,
        resCode : 401,
        resMessage : "User Unauthorized"
    }
}

router.post('/add',authenticate, function(req, res, next) {
    if(req.body.language && req.body.lang_shortkey) {
      var lang_details=new Language({
                        language        : req.body.language,
                        lang_shortkey   :req.body.lang_shortkey,
                        flag            : req.body.flag
                      });
        lang_details.save(function(err, language) {
            if (err) throw err;
            if(language) {
                var response = {
                    success : true,
                    resCode : 200,
                    resMessage : "Language added successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});

router.put('/edit/:id',authenticate, function(req, res, next) {
    if(req.body.language && req.body.lang_shortkey) {
        var details={
            language      : req.body.language,
            lang_shortkey : req.body.lang_shortkey,
            flag          : req.body.flag
        };
        Language.update({_id:req.params.id},{$set:details}, function(err, language) {
            if (err) throw err;
            if(language) {
                var response = {
                    success : true,
                    resCode : 200,
                    resMessage : "Language edited successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});

router.post('/publish',authenticate, function(req, res, next) {
    if(req.body.flag && req.body.id) {
        var details={
            flag      : req.body.flag
        };
        Language.update({_id:req.body.id},{$set:details}, function(err, language) {
            if (err) throw err;
            if(language) {
                var response = {
                    success : true,
                    text    : req.body.flag,
                    resCode : 200,
                    resMessage : "Edited successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});

router.get('/delete/:id',authenticate, function(req, res, next) {
    if(req.params.id) {
        Language.remove({_id:req.params.id},function(err, item) {
            if (err) throw err;
            if(item) {
                var response = {
                    success : true,
                    resCode : 200,
                    resMessage : "Deleted successfully"
                }
                res.status(200).json(response);
            }else {
                res.status(401).json(ERROR.UNAUTH);
            }
        });
    } else {
        res.status(400).json(ERROR.BAD_REQ);
    }
});

module.exports = router;
