var express         = require('express');
var path            = require('path');
var bodyParser      = require('body-parser');
var fs              = require('fs');
var router              = express.Router();

var app             = express();
var port            = 4000;

//include all controller
var Auth            = require('./admin/controllers/auth');
var Authed            = require('./admin/controllers/authedapi');

var Adminauth		= require('./admin/front/auth');
var Adminauthed		= require('./admin/front/authedapi');


// App configuration
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended : true,
  limit : '50mb'
}));
app.get('/public/*', function(req,res){
    console.log(req.originalUrl);
    res.sendFile(path.join(__dirname, req.originalUrl));
});
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use('/api/auth', Auth);
app.use('/api/authed',Authed);

app.use('/api/admin/auth',Adminauth);
app.use('/api/admin/authed',Adminauthed);

app.listen(port , function() {
  console.log('Server started at port ' + port);
});
