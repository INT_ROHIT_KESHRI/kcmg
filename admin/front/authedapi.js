var express = require('express');
var router = express.Router();
var app = express();
var connection = require('../../config/database');
var asyncForEach = require('async-foreach').forEach;
var mkdirp = require('mkdirp');
var multer = require('multer');
var csv = require('csv');
var path = require('path');
var fs = require('fs-extra');
/********** CSV file upload Start *************/
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname+ '-' + Date.now()+'.csv')
    }
});
var upload = multer({ storage: storage });

router.post('/uploadCSV', upload.single('file'), function(req, res, next){
    console.log(req.file.filename);
    var basePath = path.join(__dirname, '../../uploads');
    var file = path.join(__dirname, '/../../uploads/'+req.file.filename);

    var skipHeader = true; // config option
    var dataObj = {};
    var read = fs.createReadStream(file),
        //write = fs.createWriteStream(basePath+'/out.json'),
        parse = csv.parse(),
        rowCount = 0, // to keep track of where we are

        transform = csv.transform(function(row,cb) {
            var result;
            if ( skipHeader && rowCount === 0  ) { // if the option is turned on and this is the first line
                result = null; // pass null to cb to skip
            } else {
                result = JSON.stringify(row)+'\n'; // otherwise apply the transform however you want

				//connection.query('INSERT INTO users SET ?', dataObj);
                var qry = "SELECT * from users where email LIKE '%" + row[1] + "%'";
                var query = connection.query(qry, function(err, rows, fields) {
					console.log("Rows",rows);
                    if (rows.length == 0) {
                        dataObj = {
                            'name' : row[0],
                            'email' : row[1],
                            'phone' : row[2],
                            'country' : row[3]
                        };
                        connection.query('INSERT INTO users SET ?', dataObj);
                    }
                });
            }
            rowCount++; // next time we're not at the first line anymore
            cb(null,result); // let node-csv know we're done transforming
        });

    read
        .pipe(parse)
        .pipe(transform);
        // .pipe(write).once('finish',function() {
        //     // done
        // });
    res.json({
        responseCode: '200',
        responseMessage: 'successfully!'
    });
});



/*list csv*/
router.get('/listCsv', function(req, res, next) {
    /********************Get user list *****************************/
    var result = [];
    var qry = "SELECT id, name, email, phone, department, country, created  from users";
    var query = connection.query(qry, function(err, response, fields) {
		console.log('err',err);
		console.log('response',response);
        result = response;
        if (result.length > 0) {
            res.status(200).json({
                csvObj: result,
                responseCode: "200"
            });
        } else {
            res.json({
                responseCode: '201',
                responseMessage: 'Result not found!'
            });
        }
    });
    /******************** Get user list End *****************************/
});



module.exports = router;
