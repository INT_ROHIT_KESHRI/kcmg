-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2017 at 12:26 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ksbm`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `token` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_token`
--

INSERT INTO `access_token` (`id`, `user_id`, `token`, `created`, `modified`) VALUES
(3, 8, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmb28iOiJiYXIiLCJpYXQiOjE0ODYwMzIxODN9.zPKLmwQFXuIp7SxR7hvzF2685u6MagA7EegTAgvUmxM', '2017-02-02 16:13:03', '2017-02-02 16:13:03'),
(5, 11, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmb28iOiJiYXIiLCJpYXQiOjE0ODYwMzU0NzN9.bPwIeX_g5XVqOE4hTwxL2GBYrlErEXa3ZOaZ04RLTOE', '2017-02-02 17:07:53', '2017-02-02 17:07:53');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `topic_id` int(50) NOT NULL,
  `comment` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `topic_id`, `comment`, `created`, `updated`) VALUES
(1, 8, 3, 'This a comment on the topic', '2017-02-02 19:51:07', '2017-02-02 19:51:07'),
(2, 8, 3, 'This a comment 2 on the topic', '2017-02-02 19:51:08', '2017-02-02 19:51:08'),
(3, 8, 3, 'This a comment 3 on the topic', '2017-02-02 19:51:09', '2017-02-02 19:51:09'),
(4, 8, 5, 'This a comment on the topic', '2017-02-02 19:51:07', '2017-02-02 19:51:07'),
(5, 8, 4, 'This a comment 2 on the topic', '2017-02-02 19:51:08', '2017-02-02 19:51:08'),
(6, 8, 2, 'This a comment 3 on the topic', '2017-02-02 19:51:09', '2017-02-02 19:51:09');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `department` varchar(200) NOT NULL,
  `topic` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `user_id`, `department`, `topic`, `created`, `modified`) VALUES
(1, 8, 'sales', 'This is a new topic', '2017-02-02 19:12:38', '2017-02-02 19:12:38'),
(2, 8, 'sales', 'This is a new topic', '2017-02-02 19:16:42', '2017-02-02 19:16:42'),
(3, 8, 'sales', 'This is a new topic', '2017-02-02 19:26:14', '2017-02-02 19:26:14'),
(4, 11, 'sales', 'This is a new topic', '2017-02-02 19:26:26', '2017-02-02 19:26:26'),
(5, 11, 'sales', 'This is a new topic', '2017-02-02 19:26:26', '2017-02-02 19:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `department` varchar(200) NOT NULL,
  `notification_token` text NOT NULL,
  `otp` bigint(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `department`, `notification_token`, `otp`, `status`, `created`, `updated`) VALUES
(8, 'Rohit', 'rohit.keshri@indusnet.co.in', '1db2cd81f19741d67e4c7aef245a689e', '9999999999', 'sales', '', 0, '1', '2017-02-02 12:29:58', '2017-02-02 12:29:58'),
(9, 'Krishnendu', 'krish@gmail.com', '', '4444444444', 'finance', '', 0, '0', '2017-02-02 12:29:58', '2017-02-02 12:29:58'),
(10, 'Saikat', 'saikat@gmail.com', '', '1111111111', 'account', '', 0, '0', '2017-02-02 12:29:58', '2017-02-02 12:29:58'),
(11, 'Swati', 'swati.srivastav@indusnet.co.in', '073a86b3d64c716728328f285c9838a4', '3333333333', 'sales', 'dRIYajdFd1Q:APA91bHOi0s3n8X1i8XBiwhtUavnlaca41sIaYIt4llGKbkAat-YEwXCCp4xYzpVSaGCdz4y7kCjR94WaTLYfmYaHm9eqRZZe8j1mg2Ztj0ZN8PoXzvpQmMkn-ddnC2E8ojanFlXscK9', 0, '1', '2017-02-02 12:29:58', '2017-02-02 12:29:58'),
(12, 'Arindam', 'arindam@gmail.com', '', '2222222222', 'sales', '', 0, '0', '2017-02-02 12:29:58', '2017-02-02 12:29:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_token`
--
ALTER TABLE `access_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_token`
--
ALTER TABLE `access_token`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
