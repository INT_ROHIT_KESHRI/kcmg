var express = require('express');
var router = express.Router();
var app = express();
var connection = require('../../config/database');
var mail = require('../../Lib/sendmail');
var authenticate = require('../../middleware/authenticate');
var sendnotification = require('../../Lib/sendnotification');
var asyncForEach = require('async-foreach').forEach;
var multer = require('multer');
var mkdirp = require('mkdirp');
var sectorIconPath = "http://150.129.179.174:4000/public/sectoricon";

var ERROR = {
        BAD_REQ: {
            success: false,
            resCode: 400,
            resMessage: "Bad request"
        },
        UNAUTH: {
            success: false,
            resCode: 401,
            resMessage: "User Unauthorized"
        },
        INVALID: {
            success: false,
            resCode: 402,
            resMessage: "Invalid Email Address"
        }
    }
    //GET THE USER LIST
router.get('/getUserList', function(req, res, next) {
    connection.query('SELECT id,name,email FROM users', function(err, response, fields) {
        if (err) {
            res.send(err);
        } else {
            res.json({
                response: response,
                responseCode: "200"
            });
        }
    });
});

//sector wise discussion
router.post('/getSectorWiseDiscussion', authenticate, function(req, res, next) {
    var conversationObj = {};
    var convObj = {};
    var totConversation = 0;
    var email = req.body.email;
    if (!email) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT * from users where email ='" + email + "'";
        var query = connection.query(qry, function(err, rows, fields) {
            if (rows.length > 0) {
                connection.query('SELECT * FROM master_sector', function(err, response, fields) {
                    if (err) {
                        res.send('Error');
                    } else {
                        if (response.length > 0) {
                            var totSectorLen = 0;
                            var totSectorCnt = 0;
                            totSectorLen = response.length;
                            console.log('sector length', totSectorLen);
                            asyncForEach(response, function(item, index, arr) {
                                conversationObj[index] = {};
                                conversationObj[index]['sector_id'] = item.id;
                                conversationObj[index]['sector_name'] = item.sector_name;
                                conversationObj[index]['sector_icon'] = sectorIconPath + "/" + item.sector_icon;
                                console.log(rows[0].id);
                                var totConvPosted = 0;
                                var getConversationPostSql = "SELECT COUNT(*) AS totConvPosted FROM conversation WHERE createdBy = " + rows[0].id + " AND sector_id = " + item.id + " ";
                                connection.query(getConversationPostSql, function(getConversationPostErr, getConversationPostResponse, getConversationPostFields) {
                                    if (getConversationPostErr) {
                                        res.send('Error');
                                    } else {
                                        console.log('coversation response', getConversationPostResponse[0]['totConvPosted']);
                                        conversationObj[index]['postedByCnt'] = getConversationPostResponse[0]['totConvPosted'];
                                        totConversation = totConversation + parseInt(getConversationPostResponse[0]['totConvPosted']);
                                        var getTagCntSql = "SELECT COUNT(*) AS totConvTagged FROM conversation WHERE  FIND_IN_SET('" + rows[0]['id'] + "',tagedUserList) AND sector_id = " + item.id + " ";
                                        console.log(getTagCntSql);
                                        connection.query(getTagCntSql, function(getTagCntSqlErr, getTagCntSqlResponse, getTagCntSqlFields) {
                                            if (getTagCntSqlErr) {
                                                res.send('Error');
                                            } else {
                                                console.log('CONV TAG RESPONSE', getTagCntSqlResponse);
                                                conversationObj[index]['taggedCnt'] = getTagCntSqlResponse[0]['totConvTagged'];
                                                totConversation = totConversation + parseInt(getTagCntSqlResponse[0]['totConvTagged']);
                                            }
                                            convObj[index] = item.id;
                                            console.log(convObj);
                                            var objlen = Object.keys(convObj).length;
                                            console.log('total sector len', totSectorLen);
                                            console.log('total object len', objlen);
                                            if (objlen == totSectorLen) {
                                                res.json({
                                                    response: conversationObj,
                                                    totConversation: totConversation,
                                                    responseCode: "200",
                                                    path: sectorIconPath,
                                                });
                                            }
                                        });
                                    }
                                });
                            })
                        } else {
                            res.status(201).send(ERROR.INVALID);
                        }
                    }
                });
            } else {
                res.status(200).send(ERROR.INVALID);
            }
        });
    }
});

//save conversation
//*****************UPLOAD START USING ASYNC PACKAGE AND MULTER ***********//
var conversationstorage = multer.diskStorage({
    destination: function(req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/conversation/';
        mkdirp(dest, function(err) {
            console.log(dest);
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var conversationimageupload = multer({
    storage: conversationstorage
});
router.post('/saveConversation', authenticate, conversationimageupload.any(), function(req, res, next) {
    console.log(req.body);
    var sectorId = req.body.sectorId;
    var email = req.body.email;
    // var createdBy = req.body.createdBy;
    var issueTitle = req.body.issueTitle;
    var country = req.body.country;
    var tagedUserList = req.body.tagedUserList;
    var issueStatus = req.body.issueStatus;
    var issueDescription= req.body.issueDescription;
    console.log(req.files);
    if (req.files.length > 0) {
        var imageUrl = req.files[0].filename;
    } else {
        var imageUrl = "";
    }
    var conversationObj = {};
    var convObj = {};
    var totConversation = 0;
    var email = req.body.email;
    if (!sectorId || !email || !country || !issueTitle || !tagedUserList || !issueStatus || !issueDescription) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT * from users where email ='" + email + "'";
        var query = connection.query(qry, function(err, rows, fields) {
            if (rows.length > 0) {
                var all_users=tagedUserList.split(',');
                var j;
                for(j=0;j<all_users.length;j++){
                    var queryuser = "SELECT * from users where id ='" + all_users[j] + "'";
                    connection.query(qry, function(err, tagusers, fields) {
                        var message = "'"+rows[0].name+"' tagged you in a conversation.";
                        var notificationtoken = tagusers[0]['notification_token'];
                        sendnotification(message, notificationtoken);
                    });
                }
                data = {
                    sector_id: sectorId,
                    createdBy: rows[0].id,
                    issueTitle: issueTitle,
                    imageUrl: imageUrl,
                    country: country,
                    tagedUserList: tagedUserList,
                    issueDescription: issueDescription,
                    issueStatus: issueStatus
                }
                console.log(data);
                connection.query('INSERT INTO conversation SET ?', data, function(err, result) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.json({
                            response: 'Issue posted successfully.',
                            responseCode: "200"
                        });
                    }
                });
            } else {
                res.status(201).send(ERROR.INVALID);
            }
        })
    }
});
//SAVE COMMENTS ON conversation
router.post('/saveComment', authenticate, function(req, res, next) {
    console.log(req);
    var conversationId = req.body.conversationId;
    var email = req.body.email;
    var comments = req.body.comments;
    var status = req.body.status;
    if (!conversationId || !email || !comments || !status) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT * from users where email ='" + email + "'";
        var query = connection.query(qry, function(err, rows, fields) {
            if (rows.length > 0) {

                // var all_users=tagedUserList.split(',');
                // var j;
                // for(j=0;j<all_users.length;j++){
                //     var queryuser = "SELECT * from users where id ='" + all_users[j] + "'";
                //     connection.query(qry, function(err, tagusers, fields) {
                //         var message = "'"+rows[0].name+"' tagged you in a conversation.";
                //         var notificationtoken = tagusers[0]['notification_token'];
                //         sendnotification(message, notificationtoken);
                //     });
                // }

                data = {
                    conversation_id: conversationId,
                    comments: comments,
                    postedBy: rows[0].id,
                    status: status
                }
                console.log(data);
                connection.query('INSERT INTO comments SET ?', data, function(err, result) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.json({
                            response: 'Comments posted successfully.',
                            responseCode: "200"
                        });
                    }
                });
            } else {
                res.status(201).send(ERROR.INVALID);
            }
        })
    }
});
//GET THE DETAIL CONVERSATION
router.post('/getDetailConversation', authenticate, function(req, res, next) {
    console.log(req);
    var conversationId = req.body.conversationId;
    if (!conversationId) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var getConversationDetalSql = "SELECT c.comments,c.postedOn ,u.name as commentsOwner from comments as c LEFT JOIN users as u ON c.postedBy=u.id  where c.conversation_id =" + conversationId + " ";
        connection.query(getConversationDetalSql, function(getConversationDetalErr, getConversationDetalRows, getConversationDetalFields) {
            if (getConversationDetalErr) {
                res.send(getConversationDetalErr);
            } else {
                res.json({
                    response: getConversationDetalRows,
                    responseCode: "200"
                });
            }
        });
    }
});

router.post('/topic', authenticate, function(req, res, next) {
    var topic = req.body.topic;
    var id = req.body.id;
    var department = req.body.department;
    if (!topic || !id || !department) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT * from users where department ='" + department + "' And status='1' And id !='" + id + "'";
        var query = connection.query(qry, function(err, rows, fields) {
            var topicdata = {
                user_id: id,
                department: department,
                topic: topic,
                created: new Date(),
                modified: new Date()
            }
            connection.query('INSERT INTO topics SET ?', topicdata, function(err, result) {
                var response = {
                    success: true,
                    resCode: 200,
                    resMessage: "Topic Post successfully"
                }
                if (rows.length > 0) {
                    var i;
                    for (i = 0; i < rows.length; i++) {
                        var message = "New topic post in the group";
                        var notificationtoken = rows[i]['notification_token'];
                        sendnotification(message, notificationtoken);
                    }
                    res.status(200).send(response);
                } else {
                    res.status(200).send(response);
                }
            });

        });
    }
});

router.post('/topicomment', authenticate, function(req, res, next) {
    var topic_id = req.body.topic_id;
    var comment = req.body.comment;
    var id = req.body.id
    var department = req.body.department;
    if (!id || !topic_id || !comment || !department) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT * from users where department ='" + department + "' And status='1' And id !='" + id + "'";
        var query = connection.query(qry, function(err, rows, fields) {
            var topiccommentdata = {
                user_id: id,
                topic_id: topic_id,
                comment: comment,
                created: new Date(),
                updated: new Date()
            }
            connection.query('INSERT INTO comments SET ?', topiccommentdata, function(err, result) {
                var response = {
                    success: true,
                    resCode: 200,
                    resMessage: "Comment Post successfully"
                }
                if (rows.length > 0) {
                    var i;
                    for (i = 0; i < rows.length; i++) {
                        var message = "New Comment on a topic in the group";
                        var notificationtoken = rows[i]['notification_token'];
                        sendnotification(message, notificationtoken);
                    }
                    res.status(200).send(response);
                } else {
                    res.status(200).send(response);
                }
            });

        });
    }
});

router.get('/alltopicomments', authenticate, function(req, res, next) {
    var department = req.query.department;
    if (!department) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT topics.topic,topics.id as topic_id, topicuser.name as topicby , comments.comment,users.name from topics LEFT JOIN comments ON topics.id=comments.topic_id  LEFT JOIN users ON comments.user_id=users.id LEFT JOIN users as topicuser ON topics.user_id=topicuser.id where topics.department='" + department + "' ORDER BY topics.id";
        var query = connection.query(qry, function(err, rows, fields) {
            var uniqueNames = [];
            var all_result = [];
            var i = -1;
            rows.forEach(function(item, index) {
                if (uniqueNames.indexOf(item.topic_id) == -1) {
                    if (item.comment == null) {
                        var comm = '';
                    } else {
                        var comm = {
                            commentby: item.name,
                            comment: item.comment
                        }
                    }

                    var test = {
                        topic_id: item.topic_id,
                        topicby: item.topicby,
                        topic: item.topic,
                        comments: (comm == '') ? [] : [comm]
                    };
                    all_result.push(test)
                    uniqueNames.push(
                        uniqueNames.topic = item.topic,
                        uniqueNames.topic_id = item.topic_id,
                        uniqueNames.comment = [item.comment]
                    );
                    i++;
                } else {
                    var comm = {
                        commentby: item.name,
                        comment: item.comment
                    }
                    all_result[i].comments.push(comm);
                    uniqueNames.comment.push(item.comment);
                }

            });
            var response = {
                success: true,
                resCode: 200,
                resMessage: all_result
            }
            res.status(200).send(response);
        });
    }
});

router.get('/getConversationByMe', authenticate, function(req, res, next) {
    var email = req.query.email;
    var sector_id = req.query.sector_id;
    if (!email || !sector_id) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT * from users where email='" + email + "'";
        var query = connection.query(qry, function(err, rows, fields) {
            if (rows.length > 0) {
                var qry2 = "select c.id as conversation_id,c.issueDescription,c.sector_id,cu.name as createdBy,c.issueTitle,c.imageUrl,c.country from conversation as c LEFT JOIN users as cu ON c.createdBy=cu.id where c.sector_id='" + sector_id + "'  AND c.createdBy='" + rows[0]['id'] + "' ";
                connection.query(qry2, function(err, allrows, fields) {
                    var response = {
                        success: true,
                        resCode: 200,
                        resMessage: allrows
                    }
                    res.status(200).send(response);
                });
            } else {
                res.status(400).json(ERROR.BAD_REQ);
            }

        });
    }
});
router.get('/getConversationByOthers', authenticate, function(req, res, next) {
    var email = req.query.email;
    var sector_id = req.query.sector_id;
    if (!email || !sector_id) {
        res.status(400).json(ERROR.BAD_REQ);
    } else {
        var qry = "SELECT * from users where email='" + email + "'";
        var query = connection.query(qry, function(err, rows, fields) {
            if (rows.length > 0) {
                var qry2 = "select c.id as conversation_id,c.issueDescription,c.sector_id,cu.name as createdBy,c.issueTitle,c.imageUrl,c.country from conversation as c LEFT JOIN users as cu ON c.createdBy=cu.id where c.sector_id='" + sector_id + "'  AND FIND_IN_SET('" + rows[0]['id'] + "',c.tagedUserList)";
                connection.query(qry2, function(err, allrows, fields) {
                    var response = {
                        success: true,
                        resCode: 200,
                        resMessage: allrows
                    }
                    res.status(200).send(response);
                });
            } else {
                res.status(400).json(ERROR.BAD_REQ);
            }

        });
    }
});


module.exports = router;
